<footer>
  <div class="container">
    <div class="row">
      <div class="col-xl-4">
        <h4>Быстрые ссылки</h4>
        <ul>
        <?php
                        $params = array(
                            'container'=> false, // Без div обертки
                            'echo'=> false, // Чтобы можно было его предварительно вернуть
                            'items_wrap'=> '%3$s', // Разделитель элементов
                            'depth'=> 0, // Глубина вложенности
                            'theme_location' => 'bottom',
                        );
                        // Чистим все теги, кроме ссылок
                        print strip_tags(wp_nav_menu( $params ), '<li><a>' );
                        ?>
        </ul>
      </div>
      <div class="col-xl-3">
        <h4>О Профсоюзе</h4>
        <ul>
        <?php
                        $params = array(
                            'container'=> false, // Без div обертки
                            'echo'=> false, // Чтобы можно было его предварительно вернуть
                            'items_wrap'=> '%3$s', // Разделитель элементов
                            'depth'=> 0, // Глубина вложенности
                            'theme_location' => 'bottom2',
                        );
                        // Чистим все теги, кроме ссылок
                        print strip_tags(wp_nav_menu( $params ), '<li><a>' );
                        ?>
        </ul>
      </div>
      <div class="col-xl-5">
        <h4>Контакты</h4>
        <ul>
          <li>
            <img src="<?php bloginfo('template_url'); ?>/img/svg/phone_blue.svg" alt="" />
            <a href="#">(831) 462-37-71</a>
          </li>
          <li>
            <img src="<?php bloginfo('template_url'); ?>/img/svg/mail_blue.svg" alt="" />
            <a href="#">pos-nngu@mail.ru</a>
          </li>
          <li>
            <img src="<?php bloginfo('template_url'); ?>/img/svg/map-pin_blue.svg" alt="" />
            <a href="#">пр.Гагарина, 23, общежитие №5 (офис-центр), каб. 17</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="footer--bottom">
    <div class="container">
      <div class="row">
        <div class="col-xl-10 col-12">
          <span>Copyright © 2019 Университет Лобачевского</span>
        </div>
        <div class="col-xl-2 col-12">
          <div class="social">
            <a href=""><img src="<?php bloginfo('template_url'); ?>/img/svg/VK_white.svg" alt=""/></a>
            <a href=""><img src="<?php bloginfo('template_url'); ?>/img/svg/Instagram_white.svg" alt=""/></a>
            <a href=""><img src="<?php bloginfo('template_url'); ?>/img/svg/Youtube.svg" alt=""/></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
<?php wp_footer(); ?> 
<script src="<?php bloginfo('template_url'); ?>/js/main.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/vendor.js"></script>
  </body>
</html>
