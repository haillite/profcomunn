<?php get_header(); ?>

<section class="single-page">
      <div class="container">
        <div class="row">
          <div class="col-xl-12" style="margin-bottom: 30px;">
            <?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
          </div> 
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <?php $thumbnail_attributes = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' ); ?>
            <div class="col-xl-4">
            <article class="news__preview">
                <div class="news__thumbnail">
                <a href="<?php the_permalink(); ?>" class="news__link"
                ><img src="<?php echo $thumbnail_attributes[0]; ?>" alt=""
                /></a>
                </div>
                <h3 class="news__title">
                    <a href="<?php the_permalink(); ?>" class="news__link"
                    ><?php the_title(); ?></a
                    >
                </h3>
                <div class="news__info">
            <img src="<?php bloginfo('template_url'); ?>/img/svg/clock.svg" alt="" class="news__info__icon" /><span
              ><?php the_time('j.m.Y'); ?></span
            >
            <img src="<?php bloginfo('template_url'); ?>/img/svg/eye.svg" alt="" class="news__info__icon" /><span
              ><?php if(function_exists('the_views')) { the_views(); } ?></span
            >
          </div>
            <p>  <?php the_excerpt(); ?>  </p>
              </div>      
            </article>
            <?php endwhile; ?>

            <?php else: ?>
            <div class="col-xl-4">
            <article class="news__preview">
                <p>В этой категории нет записей!</p>
            </article>
            </div>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </section>




<?php get_footer(); ?>
