$(document).ready(function() {
  $(".partners-library").owlCarousel({
    loop: true,
    margin: 20,
    autoHeight: true,
    nav: false,
    dots: false,
    responsive: {
      0: {
        items: 1,
        autoHeight: true
      },
      600: {
        items: 3
      },
      1000: {
        items: 3
      }
    }
  });
});
