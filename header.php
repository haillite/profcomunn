<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title><?php echo wp_get_document_title(); ?></title>
    <meta name="theme-color" content="#fff" />
    <meta
      name="apple-mobile-web-app-status-bar-style"
      content="black-translucent"
    />
    <meta
      name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"
    />
    <link
      rel="shortcut icon"
      href="<?php bloginfo('template_url'); ?>/img/favicons/favicon.ico"
      type="image/x-icon"
    />
    <link
      rel="icon"
      sizes="16x16"
      href="<?php bloginfo('template_url'); ?>/img/favicons/favicon-16x16.png"
      type="image/png"
    />
    <link
      rel="icon"
      sizes="32x32"
      href="<?php bloginfo('template_url'); ?>/img/favicons/favicon-32x32.png"
      type="image/png"
    />
    <link
      rel="apple-touch-icon-precomposed"
      href="../img/favicons/apple-touch-icon-precomposed.png"
    />
    <link rel="apple-touch-icon" href="../img/favicons/apple-touch-icon.png" />
    <link
      rel="apple-touch-icon"
      sizes="57x57"
      href="../img/favicons/apple-touch-icon-57x57.png"
    />
    <link
      rel="apple-touch-icon"
      sizes="60x60"
      href="../img/favicons/apple-touch-icon-60x60.png"
    />
    <link
      rel="apple-touch-icon"
      sizes="72x72"
      href="../img/favicons/apple-touch-icon-72x72.png"
    />
    <link
      rel="apple-touch-icon"
      sizes="76x76"
      href="../img/favicons/apple-touch-icon-76x76.png"
    />
    <link
      rel="apple-touch-icon"
      sizes="114x114"
      href="../img/favicons/apple-touch-icon-114x114.png"
    />
    <link
      rel="apple-touch-icon"
      sizes="120x120"
      href="../img/favicons/apple-touch-icon-120x120.png"
    />
    <link
      rel="apple-touch-icon"
      sizes="144x144"
      href="../img/favicons/apple-touch-icon-144x144.png"
    />
    <link
      rel="apple-touch-icon"
      sizes="152x152"
      href="../img/favicons/apple-touch-icon-152x152.png"
    />
    <link
      rel="apple-touch-icon"
      sizes="167x167"
      href="../img/favicons/apple-touch-icon-167x167.png"
    />
    <link
      rel="apple-touch-icon"
      sizes="180x180"
      href="../img/favicons/apple-touch-icon-180x180.png"
    />
    <link
      rel="apple-touch-icon"
      sizes="1024x1024"
      href="../img/favicons/apple-touch-icon-1024x1024.png"
    />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" />
    <?php wp_head(); ?>
  </head>
  <body>
<header>
  <div class="container">
    <div class="row justify-content-between">
      <div class="col-xl-3 col-6">
        <div class="logo">
          <a href="<?php echo home_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/img/svg/logo.svg" alt="" /></a>
        </div>
      </div>
      <div class="col-xl-3 col-6">
        <div class="logo-upr">
          <img
            style="width:263px; height: 62px;"
            src="<?php bloginfo('template_url'); ?>/img/svg/logotip_UMPiVR-ITOG1.svg"
            alt=""
          />
        </div>
        <div class="hamburger">
          <div class="hamburger-box">
            <div class="hamburger-inner"></div>
          </div>
        </div>
      </div>
      <div class="col-xl-6 col-12">
        <div class="header-info">
          <div class="phone">
            <img src="<?php bloginfo('template_url'); ?>/img/svg/phone.svg" alt="" /><span
              ><a href="tel:8314623771">(831) 462-37-71</a></span
            >
            <img src="<?php bloginfo('template_url'); ?>/img/svg/mail.svg" alt="" /><span
              ><a href="mailto:pos-nngu@mail.ru"> pos-nngu@mail.ru</a></span
            >
          </div>
          <div class="location">
            <img src="<?php bloginfo('template_url'); ?>/img/svg/map-pin.svg" alt="" /><span
              ><a
                href="https://www.google.ru/maps/place/%D0%9F%D1%80%D0%BE%D1%84%D1%81%D0%BE%D1%8E%D0%B7%D0%BD%D0%B0%D1%8F+%D0%BE%D1%80%D0%B3%D0%B0%D0%BD%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D1%8F+%D1%81%D1%82%D1%83%D0%B4%D0%B5%D0%BD%D1%82%D0%BE%D0%B2+%D0%A3%D0%BD%D0%B8%D0%B2%D0%B5%D1%80%D1%81%D0%B8%D1%82%D0%B5%D1%82%D0%B0+%D0%9B%D0%BE%D0%B1%D0%B0%D1%87%D0%B5%D0%B2%D1%81%D0%BA%D0%BE%D0%B3%D0%BE/@56.2999296,43.9800595,18z/data=!4m12!1m6!3m5!1s0x4151d5bab831164b:0x57c5d14879f8754!2z0J7QsdGJ0LXQttC40YLQuNC1INCd0JPQotCjIOKEliAy!8m2!3d56.3075334!4d43.9860743!3m4!1s0x0:0xdfc786d2a6597d47!8m2!3d56.3001449!4d43.9797112"
                target="_blank"
                >пр.Гагарина, 23, общежитие №5 (офис-центр), каб. 17</a
              ></span
            >
          </div>
        </div>
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="col-xl-10 align-self-center">
        <nav>

          <div class="top-menu">
          <?php
                        $params = array(
                            'container'=> false, // Без div обертки
                            'echo'=> false, // Чтобы можно было его предварительно вернуть
                            'items_wrap'=> '%3$s', // Разделитель элементов
                            'depth'=> 0, // Глубина вложенности
                            'theme_location' => 'top',
                        );
                        // Чистим все теги, кроме ссылок
                        print strip_tags(wp_nav_menu( $params ), '<a>' );
                        ?>
          </div>
          <div class="bottom-menu">
          <?php
                        $params = array(
                            'container'=> false, // Без div обертки
                            'echo'=> false, // Чтобы можно было его предварительно вернуть
                            'items_wrap'=> '%3$s', // Разделитель элементов
                            'depth'=> 0, // Глубина вложенности
                            'theme_location' => 'middle',
                        );
                        // Чистим все теги, кроме ссылок
                        print strip_tags(wp_nav_menu( $params ), '<a>' );
          ?>
          </div>
        </nav>
      </div>
    </div>
  </div>
</header>