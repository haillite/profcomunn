<?php get_header(); ?>
<section class="single-page">
      <div class="container">
        <div class="row">
          <div class="col-xl-12">
            <?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <article>
              <h1><?php the_title(); ?></h1>
              <div class="article-info">
                <img src="<?php bloginfo('template_url'); ?>/img/svg/clock.svg" alt="" /><span><?php the_time('j.m.Y'); ?></span>
                <img src="<?php bloginfo('template_url'); ?>/img/svg/eye.svg" alt="" /><span>
                <?php if(function_exists('the_views')) { the_views(); } ?>
                </span>
                <img src="<?php bloginfo('template_url'); ?>/img/svg/tag.svg" alt="" /><span>
                  <?php the_tags('', ' '); ?>
                </span>
              </div>
              <?php the_content(); ?>    
              </div>      
            </article>
            <?php endwhile; ?>

            <?php else: ?>

            <?php endif; ?>
            
          </div>
        </div>
      </div>
    </section>



<?php get_footer(); ?>

<?php wp_footer(); ?> 